package org.ua.test.unique;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ObjectsFilter<T> {
    public static void main( String[] args ) {
        List<Integer> every5and7Unique = new ObjectsFilter<Integer>().every5and7Unique( new Integer[]{
                1, 2, 3, 4, 5, 6, 5, 8, 9, 5,
                11, 12, 13, 14, 14, 16, 17, 18, 19, 14,
                21, 22, 23, 24, 21, 26, 27, 21, 29, 21,
                31, 32, 33, 34, 35
        } );

        Collections.sort( every5and7Unique );
        System.out.print( every5and7Unique );

    }

    public List<T> every5and7Unique(T[] input) {
        List<T> every5 = filter( input, 5 );
        List<T> every7 = filter( input, 7 );

        Set<T> allNoRepeats = new HashSet<>();
        allNoRepeats.addAll( every5 );
        allNoRepeats.addAll( every7 );

        return new LinkedList<>( allNoRepeats );
    }

    public List<T> filter( T[] input, int everyN ) {
        List<T> rawResults = new LinkedList<>();
        for ( int i = everyN; i <= input.length; i = i + everyN ) {
            rawResults.add( input[ i - 1 ] );
        }
        return rawResults;
    }
}
