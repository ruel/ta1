package org.ua.test.processor;

public interface IRequestHandler<T> {
    /**
     * A thread-safe method to process a single request
     *
     * @param request - request object
     */
    void processRequests( T request ) throws Exception;
}
