package org.ua.test.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.String.format;

public class Processor<T> {
    Logger log = LoggerFactory.getLogger( Processor.class );

    private final ExecutorService threadPool;
    private final IRequestHandler<T> requestHandler;

    /**
     * For each request object calls
     * <p/>
     * IRequestHandler<T>.processRequest(o) only once in a separate thread
     * <p/>
     * When the queue is empty and all processing is finished
     * <p/>
     * no threads should exist.
     *
     * @param rh         - an object that handles requests
     * @param maxThreads - total number of threads
     */
    public Processor( IRequestHandler<T> rh, int maxThreads ) {
        threadPool = Executors.newFixedThreadPool( maxThreads );
        requestHandler = rh;
    }

    /**
     * Puts the request into a queue, does not wait
     * for the request to complete
     *
     * @param request - request object
     */
    public void addRequest( final T request ) {
        threadPool.execute( new Runnable() {
            @Override
            public void run() {
                try {
                    log.debug( "Processing request {}", request );
                    requestHandler.processRequests( request );
                } catch ( Exception e ) {
                    log.error( "Couldn't process request", e );
                    throw new RuntimeException( format( "Couldn't process request: %s", request ), e );
                }
            }
        } );
    }

    /**
     * OPTIONAL
     * Asynchronous shutdown, returns immediately.
     * Instructs the processor to stop accepting requests
     * and finish existing tasks
     *
     * @param o – if not null, notifies all waiting threads on
     *          this object upon successful shutdown
     */
    public void shutDown( Object o ) {
        threadPool.shutdown();
        o.notifyAll();
    }

    /**
     * OPTIONAL
     *
     * @return true if the processor is shut down
     */
    public boolean isShutDown() {
        return threadPool.isShutdown();
    }
}
