package org.ua.test.fibonacci;

import java.util.ArrayList;
import java.util.List;

public class FibonacciNumbersRecImpl {

    public static void main( String[] args ) {
        new FibonacciNumbersRecImpl().printSequence( 20 );
    }

    public void printSequence( int size ) {
        System.out.print( getSequence( size ) );
    }

    public List<Long> getSequence( int size ) {
        if ( size < 0 ) {
            throw new IllegalArgumentException( "Size can't be less than 0" );
        }

        return iterate( 0, size, new ArrayList<Long>() );
    }

    private ArrayList<Long> iterate( int curStep, int until, ArrayList<Long> fibNumbers ) {
        if ( curStep == until ) {
            return fibNumbers;
        }

        switch ( curStep ) {
            case 0:
                fibNumbers.add( 0L );
                break;
            case 1:
                fibNumbers.add( 1L );
                break;
            default:
                fibNumbers.add( fibNumbers.get( fibNumbers.size() - 2 ) + fibNumbers.get( fibNumbers.size() - 1 ) );
                break;
        }

        return iterate( ++curStep, until, fibNumbers );
    }
}
