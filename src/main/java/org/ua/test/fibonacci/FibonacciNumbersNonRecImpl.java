package org.ua.test.fibonacci;

import java.util.LinkedList;
import java.util.List;

public class FibonacciNumbersNonRecImpl {

    public static void main( String[] args ) {
        new FibonacciNumbersNonRecImpl().printSequence( 20 );
    }

    public void printSequence( int size ) {
        System.out.print( getSequence( size ) );
    }

    public List<Long> getSequence( int size ) {
        if ( size < 0 ) {
            throw new IllegalArgumentException( "Size can't be less than 0" );
        }

        LinkedList<Long> fibNumbers = new LinkedList<>();
        for ( int curStep = 0; curStep < size; curStep++ ) {
            switch ( curStep ) {
                case 0:
                    fibNumbers.add( 0L );
                    break;
                case 1:
                    fibNumbers.add( 1L );
                    break;
                default:
                    fibNumbers.add( fibNumbers.get( curStep - 2 ) + fibNumbers.get( curStep - 1 ) );
                    break;
            }
        }

        return fibNumbers;
    }
}
