package org.ua.test.fibonacci;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class FibonacciNumbersTest {

    private FibonacciNumbersRecImpl fibNumRec;
    private FibonacciNumbersNonRecImpl fibNumNonRec;

    @Before
    public void setUp() {
        fibNumRec = new FibonacciNumbersRecImpl();
        fibNumNonRec = new FibonacciNumbersNonRecImpl();
    }

    @Test
    public void negativeSizeSequence() {
        try {
            fibNumRec.getSequence( -2 );
            fail();
        } catch ( IllegalArgumentException expected ) {
            // expected
        }

        try {
            fibNumNonRec.getSequence( -2 );
            fail();
        } catch ( IllegalArgumentException expected ) {
            // expected
        }
    }

    @Test
    public void fibNumbersRec() {
        assertThat( fibNumRec.getSequence( 0 ), equalTo( Collections.<Long>emptyList() ) );
        assertThat( fibNumRec.getSequence( 1 ), equalTo( asList( 0L ) ) );
        assertThat( fibNumRec.getSequence( 2 ), equalTo( asList( 0L, 1L ) ) );
        assertThat( fibNumRec.getSequence( 3 ), equalTo( asList( 0L, 1L, 1L ) ) );
        assertThat( fibNumRec.getSequence( 4 ), equalTo( asList( 0L, 1L, 1L, 2L ) ) );
        assertThat( fibNumRec.getSequence( 5 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L ) ) );
        assertThat( fibNumRec.getSequence( 6 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L, 5L ) ) );
        assertThat( fibNumRec.getSequence( 7 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L, 5L, 8L ) ) );
        assertThat( fibNumRec.getSequence( 8 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L ) ) );
        assertThat( fibNumRec.getSequence( 9 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L ) ) );
        assertThat( fibNumRec.getSequence( 10 ), equalTo(asList( 0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L ) ) );
    }

    @Test
    public void fibNumbersNonRec() {
        assertThat( fibNumNonRec.getSequence( 0 ), equalTo( Collections.<Long>emptyList() ) );
        assertThat( fibNumNonRec.getSequence( 1 ), equalTo( asList( 0L ) ) );
        assertThat( fibNumNonRec.getSequence( 2 ), equalTo( asList( 0L, 1L ) ) );
        assertThat( fibNumNonRec.getSequence( 3 ), equalTo( asList( 0L, 1L, 1L ) ) );
        assertThat( fibNumNonRec.getSequence( 4 ), equalTo( asList( 0L, 1L, 1L, 2L ) ) );
        assertThat( fibNumNonRec.getSequence( 5 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L ) ) );
        assertThat( fibNumNonRec.getSequence( 6 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L, 5L ) ) );
        assertThat( fibNumNonRec.getSequence( 7 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L, 5L, 8L ) ) );
        assertThat( fibNumNonRec.getSequence( 8 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L ) ) );
        assertThat( fibNumNonRec.getSequence( 9 ), equalTo( asList( 0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L ) ) );
        assertThat( fibNumNonRec.getSequence( 10 ), equalTo(asList( 0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L ) ) );
    }
}