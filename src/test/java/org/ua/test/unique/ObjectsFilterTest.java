package org.ua.test.unique;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ObjectsFilterTest {

    @Test
    public void every5And7Unique() {
        List<Integer> every5and7Unique = new ObjectsFilter<Integer>().every5and7Unique( new Integer[]{
                1, 2, 3, 4, 5, 6, 5, 8, 9, 5,
                11, 12, 13, 14, 14, 16, 17, 18, 19, 14,
                21, 22, 23, 24, 21, 26, 27, 21, 29, 21,
                31, 32, 33, 34, 35
        } );

        Collections.sort( every5and7Unique );
        assertThat( every5and7Unique, equalTo( asList( 5, 14, 21, 35 ) ) );
    }

    @Test
    public void filterIntegers() {
        ObjectsFilter<Integer> intFilter = new ObjectsFilter<>();

        assertThat( intFilter.filter( new Integer[]{ }, 10 ), equalTo( Collections.<Integer>emptyList() ) );
        assertThat( intFilter.filter( new Integer[]{ 1, 2, 3, 4, 5, 6 }, 1 ), equalTo( asList( 1, 2, 3, 4, 5, 6 ) ) );
        assertThat( intFilter.filter( new Integer[]{ 1, 2, 1, 4, 1, 6 }, 2 ), equalTo( asList( 2, 4, 6 ) ) );
        assertThat( intFilter.filter( new Integer[]{ 1, 2, 1, 4, 1, 6 }, 3 ), equalTo( asList( 1, 6 ) ) );
        assertThat( intFilter.filter( new Integer[]{ 1, 1, 1, 1, 1, 1 }, 1 ), equalTo( asList( 1, 1, 1, 1, 1, 1 ) ) );
        assertThat( intFilter.filter( new Integer[]{ 1, 1, 1, 1, 1, 1 }, 2 ), equalTo( asList( 1, 1, 1 ) ) );
        assertThat( intFilter.filter( new Integer[]{ 1, 1, 1, 1, 1, 1 }, 3 ), equalTo( asList( 1, 1 ) ) );
    }

    @Test
    public void everyNMoreThanSize() {
        ObjectsFilter<Integer> intFilter = new ObjectsFilter<>();
        assertThat( intFilter.filter( new Integer[]{ 1 }, 10 ), equalTo( Collections.<Integer>emptyList() ) );
    }

    @Test
    public void filterContainEverything() {
        ObjectsFilter<Something> objFilter = new ObjectsFilter<>();
        assertThat(
                objFilter.filter(
                        new Something[]{
                                new Something( 1 ),
                                new Something( 2 ),
                                new Something( 3 ),
                                new Something( 2 )
                        },
                        2
                ),
                equalTo(
                        asList(
                                new Something( 2 ),
                                new Something( 2 )
                        )
                )
        );

        ObjectsFilter<SomethingWithoutEquals> objWithoutEqualsFilter = new ObjectsFilter<>();
        assertThat(
                objWithoutEqualsFilter.filter(
                        new SomethingWithoutEquals[]{
                                new SomethingWithoutEquals( 2 ),
                                new SomethingWithoutEquals( 2 ),
                                new SomethingWithoutEquals( 2 ),
                                new SomethingWithoutEquals( 2 )
                        },
                        1
                ).size(),
                equalTo( 4 )
        );
    }

    private static class Something {
        private final Integer id;

        public Something( Integer id ) {
            this.id = id;
        }

        @Override
        public boolean equals( Object o ) {
            if ( this == o ) return true;
            if ( o == null || getClass() != o.getClass() ) return false;

            Something something = ( Something ) o;

            if ( id != null ? !id.equals( something.id ) : something.id != null ) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }

        @Override
        public String toString() {
            return "MessedEqualsObject{" +
                    "id=" + id +
                    '}';
        }
    }

    private static class SomethingWithoutEquals {
        private final Integer id;

        public SomethingWithoutEquals( Integer id ) {
            this.id = id;
        }
    }
}